// @flow
import invariant from 'invariant';
import R from 'ramda';

import type {
  EnvironmentData,
  Map,
  MapItem,
  Client,
  CWP,
  ClientId,
  ElementarySector,
  SectorGroup,
  Suggestion,
  Suggestions,
} from '../types';

import getClients from './getClients';
import getClientById from './getClientById';
import getElementarySectors from '../sectors/getElementarySectors';
import getSectorGroupByElementarySectors from '../sectors/getSectorGroupByElementarySectors';
import getSectorGroupByName from '../sectors/getSectorGroupByName';


function isValidClientId(data: EnvironmentData, clientId: ClientId): boolean {
  return getClientById(data, clientId) !== null;
}

function isMapValid(
  clients: Array<Client>,
  elementarySectors: Array<ElementarySector>,
  map: Map,
): boolean {

  const getKnownCwpIds = R.pipe(
    R.filter(R.propEq('type', 'cwp')),
    R.map(R.prop('id')),
  );

  const knownCwpIds: Array<ClientId> = getKnownCwpIds(clients);


  type MapItemChecker = MapItem => boolean;
  type MapChecker = Map => boolean;

  const hasValidClientId: MapItemChecker = R.pipe(
    R.prop('clientId'),
    R.contains(R.__, knownCwpIds),
  );

  const hasValidSectors: MapItemChecker = R.pipe(
    R.propOr([], 'sectors'),
    R.both(
      // R.all will spit its guts if passed a non array
      R.isArrayLike,
      R.all(R.contains(R.__, elementarySectors)),
    ),
  );


  const isMapItemValid: MapItemChecker = R.allPass([hasValidSectors, hasValidClientId]);

  const isMapValid: MapChecker = R.all(isMapItemValid);
  return isMapValid(map);
}

export default function getSectorSuggestions(
  data: EnvironmentData,
  map: Map,
  clientId: ClientId,
): Suggestions {
  const elementarySectors = getElementarySectors(data);
  const clients = getClients(data);

  invariant(
    map && Array.isArray(map) && isMapValid(clients, elementarySectors, map),
    'Invalid argument: map',
  );

  invariant(
    clientId && isValidClientId(data, clientId),
    'Invalid argument: clientId'
  );

  type FindById = ClientId => Map => ?MapItem;
  const findById: FindById = (clientId: ClientId) => R.find(R.propEq('clientId', clientId));

  type IsCwpClosed = ClientId => Map => boolean;
  const isCwpClosed: IsCwpClosed = (clientId: ClientId) => R.pipe(
    findById(clientId),
    R.propOr([], 'sectors'),
    R.isEmpty,
  );


  return isCwpClosed(clientId)(map)
    ? _getSuggestionsOnClosedCwp(data, map, clientId)
    : _getSuggestionsOnOpenedCwp(data, map, clientId);
}

export function _getSuggestionsOnOpenedCwp(
  data: EnvironmentData,
  map: Map,
  clientId: ClientId,
): Suggestions {
  const client = getClientById(data, clientId);
  const mapItem = map.find(R.propEq('clientId', clientId));

  if(!client || client.type !== 'cwp' || !mapItem || !mapItem.sectors) {
    throw new Error('Invalid argument: clientId');
  }

  // Find sectors bound to this particular clientId
  const boundSectors = mapItem.sectors;
  const sectorGroup = getSectorGroupByElementarySectors(data, boundSectors);

  // We have an unknown sector group, do not suggest anything
  if(!sectorGroup) {
    return [];
  }

  const suggestions = sectorGroup.canAccept.map(sectorGroupName => {
    // First expand our canAccept sectorGroup
    const sectorGroup = getSectorGroupByName(data, sectorGroupName);
    if(!sectorGroup) {
      throw new Error('This error should not happen');
    }
    return [...boundSectors, ...sectorGroup.elementarySectors];
  });



  return suggestions;
}

export function _getSuggestionsOnClosedCwp(
  data: EnvironmentData,
  map: Map,
  clientId: ClientId
): Suggestions {

  type SectorGroupNameToES = string => Array<ElementarySector>;
  const sectorGroupNameToES: SectorGroupNameToES = name => {
    const sectorGroup = getSectorGroupByName(data, name);
    if(!sectorGroup) {
      return [];
    }
    return sectorGroup.elementarySectors;
  }

  const getSuggestionsForMapItem: MapItem => ?Array<?Suggestion> = R.pipe(
    // Get sectors bound to current cwp
    R.propOr([], 'sectors'),
    (elementarySectors: Array<ElementarySector>): ?Suggestions => {
      // This particular mapItem as no sectors bound, return null
      if(R.isEmpty(elementarySectors)) {
        return null;
      }
      // Try to find sectorGroup definition for this particular mapItem
      const sectorGroup = getSectorGroupByElementarySectors(data, elementarySectors);


      // Nothing in `canGive`, return bound elementary sectors
      if(!sectorGroup || !sectorGroup.canGive) {
        return [elementarySectors];
      }

      // At this point, the bound sectorGroup canGive some stuff
      // In our canGive definitions, we accept a sector group name
      // Transform this name to elementary sectors
      const canGive = sectorGroup.canGive.map(sectorGroupNameToES);

      // We also add the current elementary sectors combination
      // In other words, we assume the current sectorGroup "can give" itself
      return [elementarySectors, ...canGive];
    },
  );

  type GetGivenByRoom = Map => Suggestions;
  const getGivenByRoom: GetGivenByRoom = R.pipe(
    // Loop through the room
    // and get everything the room has to give
    R.map(getSuggestionsForMapItem),
    // Reject null stuff
    R.reject(R.isNil),
    // Flatten results
    R.unnest,
  );

  // Ok now, we have all elementary sector combination the room can give
  // We need to reject those which match `filteredSectors`
  const client = getClientById(data, clientId);

  type GetSectorsToReject = Client => Array<ElementarySector>;
  const getSectorsToReject = R.pipe(
    R.pathOr([], ['suggestions', 'filteredSectors']),
    R.map(sectorGroupNameToES),
    R.flatten,
    R.uniq,
  );


  const rejectedSectors = getSectorsToReject(client);

  type IsRejectedByFilter = Suggestion => boolean;
  const isRejectedByFilter: IsRejectedByFilter = R.any(R.contains(R.__, rejectedSectors));

  // Now we can sort those suggestions
  // Use the _makeSugestionRater helper
  const rateSuggestion: SuggestionRater = R.pipe(
    _makeSugestionRater(data, clientId),
    // The sort order is reversed, negate it
    R.negate,
  );


  const suggestions: Suggestions = R.pipe(
    getGivenByRoom,
    R.reject(isRejectedByFilter),
    R.sortBy(rateSuggestion),
  )(map);


  return suggestions;
}


type SuggestionRater = Suggestion => number;
function _makeSugestionRater(data: EnvironmentData, clientId: ClientId): SuggestionRater {
  const client = getClientById(data, clientId);

  if(!client || client.type !== 'cwp') {
    throw new Error('Invalid argument: clientId');
  }

  const preferenceOrder = client.suggestions.preferenceOrder;


  const mapIndex = R.addIndex(R.map);

  type WeightTableItem = {|
    weight: number,
    sectors: Array<ElementarySector>,
  |};

  type WeightTable = Array<WeightTableItem>;

  const weightTable: WeightTable = preferenceOrder.map((sectorGroupName, index, total) => {
    const weight = 100 * (R.length(total) - index + 10);
    const sectorGroup = getSectorGroupByName(data, sectorGroupName);

    if(!sectorGroup) {
      throw new Error('This error should not happen');
    }

    return {
      weight,
      sectors: sectorGroup.elementarySectors,
    };
  });

  return (sectors: Array<ElementarySector>): number => {
    const findWeightItem: WeightTable => ?WeightTableItem = R.pipe(
      R.find(
        R.pipe(
          // Get sectors from weighted list
          R.propOr([], 'sectors'),
          // Remove them from sectors to rate
          R.without(R.__, sectors),
          // If it's empty, that means our supplied sector is included in current weighted group
          R.isEmpty,
        ),
      ),
    );
    const weightItem = findWeightItem(weightTable);
    if(!weightItem) {
      return 0;
    }

    // We return the weight and we add more points if the provided ES combination counts multiple items
    // We want the largest items returned first
    return weightItem.weight + sectors.length;
  }
}

// @flow
import type {
  EnvironmentData,
  FlightPosition,
} from '../types';

import type {
  DestinationId,
} from '../types/xman';

import R from 'ramda';

type AreaType = 'freeze' | 'capture' | 'track';

import { isInAirspace } from '../utils/isInAirspace';

import getAreasForDestination from './getAreasForDestination';

const isInArea = (type: AreaType) => (
  data: EnvironmentData,
  destination: DestinationId,
  coords: FlightPosition,
): bool => {
  const area = getAreasForDestination(data, destination);
  if(!area[type]) {
    return false;
  }

  return isInAirspace(area[type], coords);
};


export const isTracked = R.anyPass([isInArea('track'), isInArea('capture'), isInArea('freeze')]);
export const isCaptured = R.anyPass([isInArea('capture'), isInArea('freeze')]);
export const isFrozen = isInArea('freeze');

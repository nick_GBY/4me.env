// @flow
import R from 'ramda';
import invariant from 'invariant';

import type {
  EnvironmentData,
} from '../types';

import type {
  XmanAreas,
  DestinationId,
} from '../types/xman';

const defaultReturn = {
  freeze: null,
  capture: null,
  track: null,
  sectors: null,
};

export default function getAreasForDestination(
  data: EnvironmentData,
  destination: DestinationId,
): XmanAreas {

  invariant(
    destination &&
    typeof destination === 'string' &&
    data.xman.destinations[destination],
    `${destination} is not a valid destination identifier`
  );

  return data.xman.areas[destination] || defaultReturn;
}

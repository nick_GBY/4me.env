// @flow
import R from 'ramda';
import invariant from 'invariant';

import type {
  EnvironmentData,
  FlightPosition,
  ElementarySector,
} from '../types';

import type {
  DestinationId,
  Destination,
  XmanAreas,
} from '../types/xman';

export type XmanAPI = {|
  getDestinations: void => Destination,
  getAreasForDestination: DestinationId => XmanAreas,
  isTracked: (DestinationId, FlightPosition) => bool,
  isCaptured: (DestinationId, FlightPosition) => bool,
  isFrozen: (DestinationId, FlightPosition) => bool,
  isInSector: (DestinationId, ElementarySector, FlightPosition) => bool,
  isInGeoSector: (DestinationId, ElementarySector, FlightPosition) => bool,
|};

import getDestinations from './getDestinations';
import getAreasForDestination from './getAreasForDestination';
import {
  isTracked,
  isCaptured,
  isFrozen,
} from './isInArea';

import {
  isInSector,
  isInGeoSector,
} from './isInSector';


const getXmanAPI = (data: EnvironmentData): XmanAPI => {
  invariant(
    data,
    'Invalid data provided'
  );

  const {
    xman,
  } = data;

  invariant(
    xman &&
    xman.destinations,
    'Invalid data provided',
  );

  return {
    getDestinations: getDestinations.bind(null, data),
    getAreasForDestination: getAreasForDestination.bind(null, data),
    isTracked: isTracked.bind(null, data),
    isCaptured: isCaptured.bind(null, data),
    isFrozen: isFrozen.bind(null, data),
    isInSector: isInSector.bind(null, data),
    isInGeoSector: isInGeoSector.bind(null, data),
  };
};

export default getXmanAPI;

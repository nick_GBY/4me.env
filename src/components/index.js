// @flow
import R from 'ramda';
import invariant from 'invariant';

import type {
  EnvironmentData,
  ControlRoomLayout,
} from '../types';

export type ComponentAPI = null | {|
  getControlRoomLayout: void => ControlRoomLayout,
|};

import getControlRoomLayout from './getControlRoomLayout';

const getComponentsAPI = (data: EnvironmentData): ComponentAPI => {
  invariant(
    data,
    'Invalid data provided'
  );

  if(!data.components) {
    return null;
  }

  return {
    getControlRoomLayout: getControlRoomLayout.bind(null, data),
  };
};

export default getComponentsAPI;

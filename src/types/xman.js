// @flow

export type SourceXmanData = {
  destinations: SourceDestinations,
  areas: SourceAreas,
};

export type SourceAreas = {
  [key: DestinationId]: XmanAreas,
};

export type SourceDestinations = {
  [key: DestinationId]: SourceDestination,
};

export type SourceDestination = {
  displayName: string,
  mode: XmanMode,
};

// XMAN
export type DestinationId = string;

export type Destination = SourceDestination & {
  name: DestinationId,
};

export type XmanMode = 'mach' | 'speed';

import type { ElementarySector } from './index';

export type XmanAreas = {
  track: ?AirspaceDefinition,
  capture: ?AirspaceDefinition,
  freeze: ?AirspaceDefinition,
  sectors: {
    [key: ElementarySector]: ?AirspaceDefinition,
  },
};

export type AirspaceDefinition = Array<AirspaceSlice>;
export type AirspaceSlice = {|
  altitude?: {
    min: ?number,
    max: ?number,
  },
  polygon: Array<GeoPoint>,
|};

export type GeoPoint = [
  number, // Longitude
  number, // Lattitude
];

// @flow
import type React from 'react';

// Root type

import type { SourceXmanData } from './xman';

export type EnvironmentData = {
  clusters: Array<Cluster>,
  sectorGroups: Array<SectorGroup>,
  clients: Array<Client>,
  frequencies: Array<Frequency>,
  components?: ComponentsData,
  xman: SourceXmanData,
};

type ComponentsData = {
  ControlRoomLayout: ControlRoomLayout,
};

// Frequencies
export type Frequency = {|
  name: string,
  defaultSector?: ?string,
|};

// Sectors
export type Cluster = Array<SectorBlock>;
export type SectorBlock = Array<ElementarySector>;

export type SectorGroup = {|
  name: string,
  elementarySectors: Array<ElementarySector>,
  canAccept: Array<ElementarySector>,
  canGive: Array<ElementarySector>,
|};

export type ElementarySector = string;

// Clients
export type ValidClientType = "cwp" | "supervisor" | "flow-manager" | "tech-supervisor";

export type Client = RegularClient | CWP;

export type ClientId = number;

export type RegularClient = {|
  id: ClientId,
  name: string,
  type: "supervisor" | "flow-manager" | "tech-supervisor",
|};

export type CWP = {|
  id: ClientId,
  name: string,
  type: "cwp",
  backupedRadios?: Array<string>,
  suggestions: {
    filteredSectors: Array<string>,
    preferenceOrder: Array<string>,
  },
|};

// Map
export type MapItem = {
  clientId: ClientId,
  sectors?: Array<ElementarySector>,
};

export type Map = Array<MapItem>;

// Suggestions
export type Suggestion = Array<ElementarySector>;
export type Suggestions = Array<Suggestion>;

// Coordinates
export type FlightPosition = {|
  altitude: number,
  lat: number,
  long: number,
|};


// React components
type ControlRoomLayoutProps = {
  cwpButton: React.Element<*>,
  roomStatus?: React.Element<*>,
};

type ControlRoomLayoutDefaultProps = {
  roomStatus: React.Element<*>,
};

export type ControlRoomLayout = Class<React.Component<ControlRoomLayoutDefaultProps, ControlRoomLayoutProps, *>>;

// @flow
import invariant from 'invariant';

// Import data
import LFEE from './data/LFEE';

// Import API constructor
import getSectorsAPI from './sectors';
import type { SectorAPI } from './sectors';
import getClientsAPI from './clients';
import type { ClientAPI } from './clients';
import getFrequenciesAPI from './frequencies';
import type { FrequencyAPI } from './frequencies';
import getComponentsAPI from './components';
import type { ComponentAPI } from './components';
import getXmanAPI from './xman';
import type { XmanAPI } from './xman';

export const ENVIRONMENTS = {
  LFEE: LFEE(),
};


export type KnownEnvironment = $Keys<typeof ENVIRONMENTS>;

export type MainAPI = {|
  sectors: SectorAPI,
  clients: ClientAPI,
  frequencies: FrequencyAPI,
  components?: ComponentAPI,
  xman: XmanAPI,
|};

export default function getEnv(env: KnownEnvironment) {
  invariant(
    env && typeof env === "string",
    'Please provide a valid environment identifier.',
  );

  invariant(
    Object.keys(ENVIRONMENTS).indexOf(env.toUpperCase()) !== -1,
    `${env} is not a valid environment identifier`,
  );

  const r: MainAPI = {
    sectors: getSectorsAPI(ENVIRONMENTS[env.toUpperCase()]),
    clients: getClientsAPI(ENVIRONMENTS[env.toUpperCase()]),
    frequencies: getFrequenciesAPI(ENVIRONMENTS[env.toUpperCase()]),
    xman: getXmanAPI(ENVIRONMENTS[env.toUpperCase()]),
  };

  if(ENVIRONMENTS[env.toUpperCase()].components) {
    Object.assign(r, {
      components: getComponentsAPI(ENVIRONMENTS[env.toUpperCase()]),
    });
  }

  return r;
}

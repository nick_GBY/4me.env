// @flow
export default [
  // Cluster 1 : URMN
  [
    // 5R
    ['UR', 'XR', 'KR', 'HYR'],
    // 4N
    ['UB', 'UN', 'KN', 'HN'],
  ],
  // Cluster 2 : RFUE
  [
    // 4E
    ['UE', 'XE', 'KE', 'HE'],
    // 4H
    ['UH', 'XH', 'KH', 'HH'],
    // KD + 2F
    ['KD', 'UF', 'KF'],
    // FIR
    ['E', 'SE'],
  ],
];

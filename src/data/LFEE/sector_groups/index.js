// @flow
import FOURN from './4N';
import FIVER from './5R';
import URMN from './URMN';

import RFUE from './RFUE';
import FOURE from './4E';
import FOURH from './4H';
import FIR from './FIR';
import KD2F from './KD2F';

import RMS from './RMS';

export default [
  ...FOURN,
  ...FIVER,
  ...FOURE,
  ...FOURH,
  ...FIR,
  ...KD2F,
  ...RFUE,
  ...URMN,
  ...RMS,
];

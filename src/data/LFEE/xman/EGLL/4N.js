// @flow

// XMAN LFEE EGLL [UN, UB, KN, HN] definitions

const geo = [
  [2.8298990999999996, 48.459629899999996],
  [6.006944400000001, 49.453055600000006],
  [2.043457, 51.0310311],
  [0.7697222, 50.4013889],
  [1.9720458999999995, 49.4788324],
  [2.8298990999999996, 48.459629899999996],
];

export default {
  UN: [{
    altitude: {min: 26500, max: 34500},
    polygon: geo,
  }],
  UB: [{
    altitude: {min: 26500, max: 34500},
    polygon: geo,
  }],
  KN: [{
    altitude: {min: 34500, max: 36500},
    polygon: geo,
  }],
  HN: [{
    altitude: {min: 36500, max: 66000},
    polygon: geo,
  }],
};

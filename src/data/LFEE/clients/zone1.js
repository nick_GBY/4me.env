// @flow
const zone1 = [
  {
    id: 11,
    name: 'P11',
    type: 'cwp',
    suggestions: {
      filteredSectors: [],
      preferenceOrder: [
        "RMS",
        "RFUE",
        "FIR"
      ]
    },
    backupedRadios: [
      "128.300",
      "124.950",
      "135.305",
      "128.835",
      "136.330",
    ]
  }, {
    id: 12,
    name: 'P12',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "4E",
        "4H",
        "KD2F"
      ],
      preferenceOrder: [
        "SE",
        "URMN"
      ]
    },
    backupedRadios: [
      "128.300",
      "124.950",
    ]
  }, {
    id: 13,
    name: 'P13',
    type: 'cwp',
    suggestions: {
      filteredSectors: [],
      preferenceOrder: []
    },
    backupedRadios: [
      "128.300",
      "124.950",
      "132.390",
      "134.405",
      "128.835",
      "133.005",
      "126.285",
    ]
  }, {
    id: 14,
    name: 'P14',
    type: 'cwp',
    suggestions: {
      filteredSectors: [
        "URMN",
        "4E",
        "4H",
        "FIR"
      ],
      preferenceOrder: []
    },
    backupedRadios: [
      "132.880",
      "132.280",
      "135.305",
    ]
  }, {
    id: 15,
    name: 'P15',
    type: 'cwp',
    backupedRadios: [],
    suggestions: {
      filteredSectors: [],
      preferenceOrder: []
    }
  }, {
    id: 16,
    name: 'P16',
    type: 'cwp',
    backupedRadios: [],
    suggestions: {
      filteredSectors: [],
      preferenceOrder: []
    }
  },
];

export default zone1;

// @flow
const zone2 = [
  {
    id: 20,
    name: 'P20',
    type: 'cwp',
    backupedRadios: [
      '132.880',
      '135.305',
      '132.280',
      '134.960',
      '134.405',
      '133.415',
      '128.835',
    ],
    suggestions: {
      filteredSectors: ["URMN"],
      preferenceOrder: ["KD", "2F"]
    },
  }, {
    id: 21,
    name: 'P21',
    type: 'cwp',
    backupedRadios: [
      '134.405',
      '133.415',
      '133.830',
    ],
    suggestions: {
      filteredSectors: ["URMN"],
      preferenceOrder: ["4H"]
    },
  }, {
    id: 22,
    name: 'P22',
    type: 'cwp',
    backupedRadios: [
      '124.950',
      '128.300',
      '127.555',
      '134.960',
      '134.405',
      '133.415',
      '133.830',
      '128.835',
    ],
    suggestions: {
      filteredSectors: ["URMN"],
      preferenceOrder: ["4H", "5EH"]
    },
  }, {
    id: 23,
    name: 'P23',
    type: 'cwp',
    backupedRadios: [
      '124.950',
      '128.300',
      '133.830',
      '128.835',
    ],
    suggestions: {
      filteredSectors: ["URMN", "KD2F"],
      preferenceOrder: ["FIR"]
    },
  }, {
    id: 24,
    name: 'P24',
    type: 'cwp',
    backupedRadios: [
      '132.390',
      '131.085',
      '134.960',
      '132.280',
    ],
    suggestions: {
      filteredSectors: ["URMN"],
      preferenceOrder: ["KD2F", "4E"]
    },
  }, {
    id: 25,
    name: 'P25',
    type: 'cwp',
    backupedRadios: [
      '127.555',
      '132.880',
      '132.390',
      '131.085',
      '134.960',
      '132.280',
    ],
    suggestions: {
      filteredSectors: ["URMN"],
      preferenceOrder: ["5E", "4EH"]
    },
  }, {
    id: 26,
    name: 'P26',
    type: 'cwp',
    backupedRadios: [
      '127.555',
      '132.880',
      '132.390',
      '131.085',
    ],
    suggestions: {
      filteredSectors: ["URMN"],
      preferenceOrder: ["5E", "4EH"]
    },
  }, {
    id: 27,
    name: 'P27',
    type: 'cwp',
    backupedRadios: [
      '127.555',
      '132.880',
      '132.390',
      '131.085',
      '134.960',
      '132.280',
      '135.305',
    ],
    suggestions: {
      filteredSectors: ["URMN", "4H", "FIR"],
      preferenceOrder: ["KD2F"]
    },
  },
];

export default zone2;

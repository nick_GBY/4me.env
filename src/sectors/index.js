// @flow
import R from 'ramda';
import invariant from 'invariant';

import type {
  EnvironmentData,
  Cluster,
  SectorGroup,
  ElementarySector,
} from '../types';

import getPrettyName from './prettyName';

export type PrettyName =
  & ((Array<ElementarySector>) => string)
  & ((Array<ElementarySector>, string) => string)
;

export type GetSectorGroupByElementarySectors = ElementarySector[] => ?SectorGroup;
export type GetSectorGroups = void => SectorGroup[];

export type SectorAPI = {|
  getElementarySectors: void => ElementarySector[],
  getClusters: void => Cluster[],
  getSectorGroups: GetSectorGroups,
  getSectorGroupByName: string => ?SectorGroup,
  getSectorGroupByElementarySectors: ElementarySector[] => ?SectorGroup,
  prettyName: PrettyName,
|};

import getSectorGroupByName from './getSectorGroupByName';
import getSectorGroups from './getSectorGroups';
import prettyName from './prettyName';
import getElementarySectors from './getElementarySectors';
import getSectorGroupByElementarySectors from './getSectorGroupByElementarySectors';
import getClusters from './getClusters';

const getSectorsAPI = (data: EnvironmentData): SectorAPI => {
  invariant(
    data,
    'Invalid data provided'
  );

  const {
    clusters,
    sectorGroups,
  } = data;

  invariant(
    clusters && sectorGroups,
    'Invalid data provided',
  );

  return {
    getElementarySectors: getElementarySectors.bind(null, data),
    getClusters: getClusters.bind(null, data),
    getSectorGroups: getSectorGroups.bind(null, data),
    getSectorGroupByName: getSectorGroupByName.bind(null, data),
    getSectorGroupByElementarySectors: getSectorGroupByElementarySectors.bind(null, data),
    prettyName: prettyName.bind(null, data),
  };
};

export default getSectorsAPI;

// @flow
import R from 'ramda';

import type {
  EnvironmentData,
  Cluster,
} from '../types';

export default function getClusters(
  data: EnvironmentData,
): Array<Cluster> {
  return data.clusters;
}

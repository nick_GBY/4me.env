// @flow
import R from 'ramda';
import invariant from 'invariant';

import type {
  EnvironmentData,
  SectorGroup,
} from '../types';

export default function getSectorGroupByName(data: EnvironmentData, name: string): ?SectorGroup {
  invariant(
    name && typeof name === 'string',
    'Invalid argument',
  );

  type FindByName = string => Array<SectorGroup> => ?SectorGroup;

  const findByName: FindByName = (name: string) => R.find(R.propEq('name', name));

  return findByName(name)(data.sectorGroups);
}

// @flow
import invariant from 'invariant';
import R from 'ramda';
import { polygon, point, inside } from '@turf/turf';

import type {
  AirspaceDefinition,
  AirspaceSlice,
} from '../types/xman';

import type {
  FlightPosition,
} from '../types';

export const isValidAirspaceDefinition = (airspace: AirspaceDefinition): bool => {
  invariant(
    airspace && Array.isArray(airspace),
    'Invalid airspace definition : not an array',
  );

  airspace.forEach(slice => {
    invariant(
      slice && slice.polygon && Array.isArray(slice.polygon),
      'Invalid airspace definition: polygon is not an array'
    );

    // If we have altitude provided, check if we have numbers
    if(slice.altitude && slice.altitude.min) {
      invariant(
        typeof slice.altitude.min === 'number',
        'Invalid airspace definition: minimum altitude is not a number',
      );
    }

    if(slice.altitude && slice.altitude.max) {
      invariant(
        typeof slice.altitude.max === 'number',
        'Invalid airspace definition: maximum altitude is not a number',
      );
    }

    // Try to generate a turf polygon from supplied data
    // If it fails, embed turf exception message in our exception
    try {
      polygon([slice.polygon]);
    } catch(err) {
      invariant(
        false,
        `Invalid airspace definition: ${err.message}`
      );
    }
  });

  return true;
};

export const isValidFlightPosition = (position: FlightPosition): bool => {
  invariant(
    position &&
    Number.isFinite(position.altitude) &&
    Number.isFinite(position.long) &&
    Number.isFinite(position.lat),
    'Invalid flight position',
  );

  const p = [position.long, position.lat];

  // The turf implementation does nothing that we do
  // Let's prevent this useless call

  // try {
  //   point(p);
  // } catch(err) {
  //   invariant(
  //     false,
  //     `Invalid flight position: ${err.message}`
  //   );
  // }

  return true;
};

export const isInAirspace = (airspace: AirspaceDefinition, position: FlightPosition, checkAltitude: bool = true): bool => {
  invariant(
    airspace &&
    isValidAirspaceDefinition(airspace),
    'Invalid airspace definition',
  );

  invariant(
    position &&
    isValidFlightPosition(position),
    'Invalid flight position',
  );

  // At this point we have a valid airspace as an array of airspace slices
  // We also have a valid flight position
  // Let's check whether the flight is included in at least one


  const isFlightInAirspace = (position: FlightPosition) => (slice: AirspaceSlice): bool => {
    // First, check altitude
    const min = (slice.altitude && slice.altitude.min) || 0;
    const max = (slice.altitude && slice.altitude.max) || Infinity;

    // Altitude check is inclusive
    // If our airspace definition has a min at 1000ft, then a flight cruising at 1000ft is included
    if(checkAltitude && (position.altitude < min || position.altitude > max)) {
      return false;
    }

    const p = point([position.long, position.lat]);
    const poly = polygon([slice.polygon]);


    return inside(p, poly);
  };

  return R.any(isFlightInAirspace(position), airspace);
};

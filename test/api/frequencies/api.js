import getFrequenciesAPI from '../../../src/frequencies';

import { frequencies } from './mock';

const expectedApi = {
  getFrequencies: 'function',
};

describe('getSectorsAPI', () => {
  test('should reject invalid data', () => {
    expect(() => getFrequenciesAPI()).toThrow();
    expect(() => getFrequenciesAPI('string')).toThrow();
    expect(() => getFrequenciesAPI({})).toThrow();
  });

  const API = getFrequenciesAPI({frequencies});

  test('should return an object', () => {
    expect(typeof API).toBe('object');
  });

  Object.keys(expectedApi).forEach(apiMethod => {
    describe(`${apiMethod}`, () => {
      test('should exist', () => {
        expect(API[apiMethod]).toBeDefined();
      });

      test(`should be a ${expectedApi[apiMethod]}`, () => {
        expect(typeof API[apiMethod]).toBe(expectedApi[apiMethod]);
      });
    });
  });
});

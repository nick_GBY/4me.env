import getSectorsAPI from '../../../src/sectors';
import R from 'ramda';

import { clusters, sectorGroups } from './mock';

describe('sector groups', () => {
  const {
    getSectorGroupByName,
    getSectorGroupByElementarySectors,
    prettyName,
  } = getSectorsAPI({clusters, sectorGroups});

  describe('getSectorGroupByName', () => {
    test('should reject invalid arguments', () => {
      expect(() => getSectorGroupByName()).toThrow(/argument/i);
      expect(() => getSectorGroupByName(null)).toThrow(/argument/i);
      expect(() => getSectorGroupByName({})).toThrow(/argument/i);
    });

    test('should accept strings as argument', () => {
      expect(() => getSectorGroupByName('string')).not.toThrow();
    });

    test('should return undefined for unknown items', () => {
      expect(getSectorGroupByName('unknown sector')).not.toBeDefined();
    });

    test('should return the proper item with an existing name', () => {
      const sectorName = 'UXR';
      const sectorGroup = getSectorGroupByName(sectorName);

      expect(sectorGroup).toBeDefined();
    });
  });

  describe('getSectorGroupByElementarySector', () => {
    test('should reject invalid arguments', () => {
      expect(() => getSectorGroupByElementarySectors()).toThrow(/argument/i);
      expect(() => getSectorGroupByElementarySectors(null)).toThrow(/argument/i);
      expect(() => getSectorGroupByElementarySectors({})).toThrow(/argument/i);
      expect(() => getSectorGroupByElementarySectors([])).toThrow(/argument/i);
      expect(() => getSectorGroupByElementarySectors(['unknown sector'])).toThrow(/argument/i);
    });

    test('should accept valid argument', () => {
      expect(() => getSectorGroupByElementarySectors(['UR'])).not.toThrow();
      expect(() => getSectorGroupByElementarySectors(['E', 'SE', 'UR'])).not.toThrow();
    });

    test('should return undefined for unknown sector groups', () => {
      expect(getSectorGroupByElementarySectors(['E', 'SE', 'UR'])).not.toBeDefined();
    });

    test('should return matching sector group', () => {
      const sectorGroup = getSectorGroupByElementarySectors(['UR', 'XR']);
      expect(sectorGroup).toBeDefined();
      expect(sectorGroup.name).toBe('UXR');
    });

    test('should return the same item regardless of argument order', () => {
      expect(getSectorGroupByElementarySectors(['UR', 'XR']))
        .toBe(getSectorGroupByElementarySectors(['XR', 'UR']));
    });
  });

  describe('prettyName', () => {
  });
});

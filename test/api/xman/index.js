import getXmanAPI from '../../../src/xman';

import { xman } from './mock';

const expectedApi = {
  getDestinations: 'function',
  getAreasForDestination: 'function',
  isTracked: 'function',
  isCaptured: 'function',
  isFrozen: 'function',
  isInSector: 'function',
  isInGeoSector: 'function',
};

describe('getXmanAPI', () => {
  test('should reject invalid data', () => {
    expect(() => getXmanAPI()).toThrow();
    expect(() => getXmanAPI('string')).toThrow();
    expect(() => getXmanAPI({})).toThrow();
  });

  const API = getXmanAPI({xman});

  test('should return an object', () => {
    expect(typeof API).toBe('object');
  });

  Object.keys(expectedApi).forEach(apiMethod => {
    describe(`${apiMethod}`, () => {
      test('should exist', () => {
        expect(API[apiMethod]).toBeDefined();
      });

      test(`should be a ${expectedApi[apiMethod]}`, () => {
        expect(typeof API[apiMethod]).toBe(expectedApi[apiMethod]);
      });
    });
  });
});

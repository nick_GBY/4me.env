export const destinations = {
  'EGLL': {
    displayName: 'London Heathrow',
    mode: 'mach',
  },
  'LSZH': {
    displayName: 'Zurich',
    mode: 'speed',
  }
};

export const areas = {
  EGLL: {
    track: [{
      polygon: [
        [0, 0],
        [1, 0],
        [1, 1],
        [0, 1],
        [0, 0],
      ],
    }],
    capture: [{
      polygon: [
        [1, 1],
        [2, 1],
        [2, 2],
        [1, 2],
        [1, 1],
      ],
    }],
    freeze: [{
      polygon: [
        [2, 2],
        [3, 2],
        [3, 3],
        [2, 3],
        [2, 2],
      ],
    }],
    sectors: {
      UR: [{
        altitude: {min: 26500, max: 34500},
        polygon: [
          [0, 0],
          [1, 0],
          [1, 1],
          [0, 1],
          [0, 0],
        ]
      }],
    },
  },
  LSZH: {},
};

export const xman = {
  destinations,
  areas,
};

test(() => {});

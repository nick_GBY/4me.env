import getEnv, { ENVIRONMENTS } from '../../src';


describe('default export', () => {
  test('should be a function', () => {
    expect(typeof require('../../src').default).toBe('function');
  });

  test('should throw for invalid input', () => {
    expect(() => getEnv()).toThrow();
    expect(() => getEnv({})).toThrow();
  });

  test('should throw for unknown environments', () => {
    expect(() => getEnv('unknown env')).toThrow();
  });

  const sampleEnv = Object.keys(ENVIRONMENTS)[0];

  test('should not throw for known environments', () => {
    expect(() => getEnv(sampleEnv)).not.toThrow();
  });

  test('should be case insensitive', () => {
    expect(() => getEnv(sampleEnv.toLowerCase())).not.toThrow();
  });

  const expectedApi = {
    sectors: 'object',
    clients: 'object',
    frequencies: 'object',
  };

  Object.keys(ENVIRONMENTS).forEach(env => {
    describe(`${env}`, () => {
      const API = getEnv(env);

      test('should return an object', () => {
        expect(typeof API).toBe('object');
      });

      Object.keys(expectedApi).forEach(apiMethod => {
        describe(`${apiMethod}`, () => {
          test('should exist', () => {
            expect(API[apiMethod]).toBeDefined();
          });

          test(`should be a ${expectedApi[apiMethod]}`, () => {
            expect(typeof API[apiMethod]).toBe(expectedApi[apiMethod]);
          });
        });
      });
    });
  });

});

import getEnv from '../../../src';

const overRLPAtFL = (flightLevel) => ({
  altitude: flightLevel * 100,
  lat: 47.945527,
  long: 5.297294,
});

const overSwitzerlandAtFL = (flightLevel) => ({
  altitude: flightLevel * 100,
  lat: 46.764191,
  long: 7.438866,
});

const overLMGAtFL = (flightLevel) => ({
  altitude: flightLevel * 100,
  lat: 45.863537,
  long: 1.182459,
});

const overMTDAtFL = (flightLevel) => ({
  altitude: flightLevel * 100,
  lat: 49.651012,
  long: 2.571978,
});

const overLULAtFL = (flightLevel) => ({
  altitude: flightLevel * 100,
  lat: 47.824650,
  long: 6.374096,
});

const overHOCAtFL = (flightLevel) => ({
  altitude: flightLevel * 100,
  lat: 47.448088,
  long: 7.658504,
});

describe('LFEE > XMAN > EGLL', () => {
  const { xman } = getEnv('LFEE');
  describe('a flight at RLP', () => {
    const flight = overRLPAtFL(340);

    test('should be tracked', () => {
      expect(xman.isTracked('EGLL', flight)).toBe(true);
    });

    test('should be captured', () => {
      expect(xman.isCaptured('EGLL', flight)).toBe(true);
    });

    test('should not be frozen', () => {
      expect(xman.isFrozen('EGLL', flight)).toBe(false);
    });

    test('should be in UF', () => {
      expect(xman.isInSector('EGLL', 'UF', flight)).toBe(true);
    });
  });

  describe('a flight over LMG', () => {
    const flight = overLMGAtFL(340);

    test('should not be tracked', () => {
      expect(xman.isTracked('EGLL', flight)).toBe(false);
    });

    const sectors = getEnv('LFEE').sectors.getElementarySectors();
    sectors.forEach(sector => {
      test(`should not be in ${sector}`, () => {
        expect(xman.isInSector('EGLL', sector, flight)).toBe(false);
      });
    });

  });

  describe('a flight over HOC', () => {
    const flight = overHOCAtFL(340);

    test('should be tracked', () => {
      expect(xman.isTracked('EGLL', flight)).toBe(true);
    });

    test('should not be captured', () => {
      expect(xman.isCaptured('EGLL', flight)).toBe(false);
    });
  });

  describe('a flight over MTD', () => {
    const flight = overMTDAtFL(340);

    test('should be tracked', () => {
      expect(xman.isTracked('EGLL', flight)).toBe(true);
    });

    test('should be captured', () => {
      expect(xman.isCaptured('EGLL', flight)).toBe(true);
    });

    test('should be frozen', () => {
      expect(xman.isFrozen('EGLL', flight)).toBe(true);
    });
  });
});

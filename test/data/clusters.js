import getEnv, { ENVIRONMENTS } from '../../src';
import R from 'ramda';

describe('cluster data', () => {
  Object.keys(ENVIRONMENTS).forEach(env => {
    describe(`${env}`, () => {
      const {
        getElementarySectors,
        getClusters,
      } = getEnv(env).sectors;

      describe('getElementarySectors', () => {
        test('should return a valid shaped object', () => {
          const elementarySectors = getElementarySectors();

          expect(Array.isArray(elementarySectors)).toBe(true);
          elementarySectors.forEach(sector => expect(typeof sector).toBe('string'));
        });
      });

      describe('getClusters', () => {
        test('should return a valid shaped object', () => {
          /**
           * Valid shape is a multilevel nested array
           * We expect an array of clusters.
           * Each cluster is an array of subclusters.
           * Each subcluster is an array of elementary sectors
           */
          const clusters = getClusters();
          expect(Array.isArray(clusters)).toBe(true);
          clusters.forEach(cluster => {
            expect(Array.isArray(cluster)).toBe(true);
            cluster.forEach(subcluster => {
              expect(Array.isArray(subcluster)).toBe(true);
              subcluster.forEach(sector => {
                expect(typeof sector).toBe('string');
              });
            });
          });
        });
      });
    });
  });
});

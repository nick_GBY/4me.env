import getEnv, { ENVIRONMENTS } from '../../src';
import R from 'ramda';

describe('sector group data', () => {
  Object.keys(ENVIRONMENTS).forEach(env => {
    describe(`${env}`, () => {
      const {
        getSectorGroups,
        getSectorGroupByName,
        getElementarySectors,
      } = getEnv(env).sectors;

      const sectorGroups = getSectorGroups();

      const sectorGroupsKeyedByName = R.groupBy(
        R.prop('name'),
        sectorGroups,
      );

      const elementarySectors = getElementarySectors();

      elementarySectors.forEach(elementarySector => {
        test(`${elementarySector} should be defined in sector groups`, () => {
          expect(sectorGroupsKeyedByName[elementarySector]).toBeDefined();
        });
      });

      sectorGroups.forEach(sectorGroup => {
        test('should have a valid shape', () => {
          expect(Object.keys(sectorGroup)).toContain('elementarySectors');
          expect(Object.keys(sectorGroup)).toContain('name');
          expect(Object.keys(sectorGroup)).toContain('canAccept');
          expect(Object.keys(sectorGroup)).toContain('canGive');
        });

        test('should have a valid name', () => {
          expect(typeof sectorGroup.name).toBe('string');
          expect(sectorGroup.name.length).not.toBe(0);
        });

        describe(`${sectorGroup.name}`, () => {
          describe('name', () => {
            test('should be unique', () => {
              expect(sectorGroupsKeyedByName[sectorGroup.name].length).toBe(1);
            });
          });

          describe('elementarySectors', () => {
            const knownElementarySectors = getElementarySectors();

            test('should be an array of strings', () => {
              expect(Array.isArray(sectorGroup.elementarySectors)).toBe(true);
            });

            test('should reference at least one elementary sector', () => {
              expect(sectorGroup.elementarySectors.length).not.toBe(0);
            });

            test('should reference defined elementary sectors', () => {
              const referencedElementarySectors = sectorGroup.elementarySectors;

              expect(
                R.without(knownElementarySectors, referencedElementarySectors)
              ).toEqual([]);
            });
          });

          const knownSectorGroupNames = R.map(
            R.prop('name'),
            sectorGroups,
          );

          describe('canAccept', () => {
            test('should reference known sector groups', () => {
              const referencedSectorGroups = sectorGroup.canAccept;
              referencedSectorGroups.forEach(referencedSectorGroup => {
                expect(knownSectorGroupNames).toContain(referencedSectorGroup);
              });
            });
          });

          describe('canGive', () => {
            test('should reference known sector groups', () => {
              const referencedSectorGroups = sectorGroup.canGive;
              referencedSectorGroups.forEach(referencedSectorGroup => {
                expect(knownSectorGroupNames).toContain(referencedSectorGroup);
              });
            });
          });
        });
      });
    });
  })
});

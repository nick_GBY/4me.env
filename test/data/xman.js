import getEnv, { ENVIRONMENTS } from '../../src';
import R from 'ramda';

import { polygon } from '@turf/turf';

import { isValidAirspaceDefinition } from '../../src/utils/isInAirspace';

describe('xman data', () => {
  Object.keys(ENVIRONMENTS).forEach(env => {
    describe(`${env}`, () => {
      const {
        getDestinations,
        getAreasForDestination,
      } = getEnv(env).xman;

      describe('getDestinations', () => {
        const destinations = getDestinations();


        test('should return an array', () => {
          expect(Array.isArray(destinations)).toBe(true);
        });

        test('each xman destination should have an ICAO airport code as a name', () => {
          destinations.forEach(destination => {
            expect(Object.keys(destination)).toContain('name');
            expect(destination.name).toMatch(/^[A-Z]{4}$/);
          });
        });

        destinations.forEach(destination => {
          describe(`${destination.name}`, () => {
            test('should have a unique name', () => {
              const knownNames = R.map(R.prop('name'), destinations);
              expect(R.indexOf(destination.name, knownNames)).toBe(R.lastIndexOf(destination.name, knownNames));
            });

            test('should have valid mode', () => {
              const VALID_MODES = ['speed', 'mach'];
              expect(Object.keys(destination)).toContain('mode');
              expect(VALID_MODES).toContain(destination.mode);
            });
          });
        });
      });

      const destinationIdentifiers = R.map(R.prop('name'), getDestinations());

      destinationIdentifiers.forEach(destination => {
        describe(`getAreasForDestination(${destination})`, () => {
          const areas = getAreasForDestination(destination);

          const AREAS = ['track', 'capture', 'freeze'];

          AREAS.forEach(area => {
            test(`should contain a ${area} area`, () => {
              expect(Object.keys(areas)).toContain(area);
            });

            if(areas[area] === null) {
              console.warn(`${env}/XMAN/${destination}: No ${area} area defined !`);
              return;
            }

            test(`should contain a valid ${area} area`, () => {
              expect(isValidAirspaceDefinition(areas[area])).toBe(true);
            });
          });

          // Now that we have checked general purpose areas are well defined, move to sector areas

          if(!areas.sectors) {
            console.warn(`${env}/XMAN/${destination}: No sector specific areas defined`);
            return;
          }

          const sectors = getEnv(env).sectors.getElementarySectors();

          sectors.forEach(sector => {
            describe(`${sector}`, () => {
              if(!areas.sectors[sector]) {
                console.warn(`${env}/XMAN/${destination}/${sector}: No sector specific area defined`);
                return;
              }

              const area = areas.sectors[sector];

              test('should be a valid area', () => {
                expect(isValidAirspaceDefinition(area)).toBe(true);
              });
            });
          });

        });
      });

    });
  });
});

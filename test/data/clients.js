import getEnv, { ENVIRONMENTS } from '../../src';
import R from 'ramda';

describe('cluster data', () => {
  Object.keys(ENVIRONMENTS).forEach(env => {
    describe(`${env}`, () => {
      const {
        getClients,
      } = getEnv(env).clients;

      describe('getClients', () => {
        const clients = getClients();

        test('should return an array', () => {
          expect(Array.isArray(clients)).toBe(true);
        });

        test('each client should have a numeric id', () => {
          clients.forEach(client => {
            expect(Object.keys(client)).toContain('id');
            expect(typeof client.id).toBe('number');
          });
        });

        clients.forEach(client => {
          describe(`${client.id}`, () => {
            test('should have a unique id', () => {
              const knownIds = R.map(R.prop('id'), clients);
              expect(R.indexOf(client.id, knownIds)).toBe(R.lastIndexOf(client.id, knownIds));
            });

            test('should have a name', () => {
              expect(Object.keys(client)).toContain('name');
              expect(typeof client.name).toBe('string');
            });

            test('should have a unique name', () => {
              const knownIds = R.map(R.prop('name'), clients);
              expect(R.indexOf(client.name, knownIds)).toBe(R.lastIndexOf(client.name, knownIds));
            });

            test('should have a valid type', () => {
              const VALID_TYPES = ['cwp', 'supervisor', 'flow-manager', 'tech-supervisor'];
              expect(VALID_TYPES).toContain(client.type);
            });
          });
        });

        const cwps = R.filter(
          R.propEq('type', 'cwp'),
          getClients(),
        );

        cwps.forEach(cwp => {
          describe(`cwp : ${cwp.id}`, () => {
            test('should have a backupedRadios prop', () => {
              expect(cwp.backupedRadios).toBeDefined();
            });

            test('should have an array as backupedRadios', () => {
              expect(R.isArrayLike(cwp.backupedRadios)).toBe(true);
            });

            const { backupedRadios } = cwp;
            const frequencies = R.map(
              R.prop('name'),
              getEnv(env).frequencies.getFrequencies(),
            );
            backupedRadios.forEach(radio => {
              describe(`${radio}`, () => {
                test('should be defined in frequencies API', () => {
                  expect(R.indexOf(radio, frequencies)).not.toBe(-1);
                });
              });
            });

            const { suggestions } = cwp;

            const sectorGroups = getEnv(env).sectors.getSectorGroups();

            const knownSectorGroupNames = R.map(
              R.prop('name'),
              sectorGroups,
            );

            if(suggestions) {
              test('should have a properly formatted suggestions object', () => {
                expect(suggestions.filteredSectors).toBeDefined();
                expect(suggestions.preferenceOrder).toBeDefined();
              });

              const { filteredSectors, preferenceOrder } = suggestions;

              filteredSectors.forEach(sector => {
                test(`${sector} should be a defined sector group`, () => {
                  expect(knownSectorGroupNames).toContain(sector);
                });
              });

              preferenceOrder.forEach(sector => {
                test(`${sector} should be a defined sector group`, () => {
                  expect(knownSectorGroupNames).toContain(sector);
                });
              });
            }

          });
        });
      });

    });
  });
});

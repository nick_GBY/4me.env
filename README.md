# 4me.env

[![build status](https://gitlab.com/devteamreims/4me.env/badges/master/build.svg)](https://gitlab.com/devteamreims/4me.env/commits/master)
[![coverage report](https://gitlab.com/devteamreims/4me.env/badges/master/coverage.svg)](https://gitlab.com/devteamreims/4me.env/commits/master)

This library provides environment data to applications in the 4ME ecosystem.

## Usage

Example :
```JavaScript
import getEnv from '4me.env';

const env = getEnv('LFEE');
// {sectors: [Object object]}
```

## Known environments

This library supports multiple environments definition as 4ME is deployed in multiple places.

Known environments are :
* `LFEE` : Reims UAC

## Nested APIs
### `sectors` API
```JavaScript
import getEnv from '4me.env';
const sectors = getEnv('LFEE').sectors;
```
Full API in [docs/sectors.md](docs/sectors.md)

### `clients` API
```JavaScript
import getEnv from '4me.env';
const clients = getEnv('LFEE').clients;
```
Full API in [docs/clients.md](docs/clients.md)

### `frequencies` API
```JavaScript
import getEnv from '4me.env';
const frequencies = getEnv('LFEE').frequencies;
```
Full API in [docs/frequencies.md](docs/frequencies.md)

### `components` API
```JavaScript
import getEnv from '4me.env';
const frequencies = getEnv('LFEE').components;
```
> Note: This API is only available is `react` is available to `require`. This allows the use of this libary on the backend without having to install `react` as a dependency.
Full API in [docs/components.md](docs/components.md)

### `XMAN` API
```JavaScript
import getEnv from '4me.env';
const frequencies = getEnv('LFEE').xman;
```
Full API in [docs/xman.md](docs/xman.md)

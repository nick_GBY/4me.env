# 0.4.3 (2017-01-06)
  * Do not throw when lat === 0 or long === 0 in isInAirspace

# 0.4.2 (2017-01-06)
  * Rename flow files to be included in the library

# 0.4.1 **BROKEN** (2017-01-06)
  * Use prepublish hook to build library and prevent publishing empty stuff

# 0.4.0 **BROKEN** (2017-01-06)
  * Implement XMAN API (see docs/xman) (devteamreims/4ME#158)
  * Correct typo in error
  * Implement React Storybook for easier development process for <ControlRoomLayout /> (devteamreims/4ME#175)

# 0.3.1 (2016-12-12)
  * Remove React hard dependency (see docs/components)

# 0.3.0 (2016-12-10)
  * Refactor build process to ouput stuff in lib/ folder

# 0.2.4 (2016-12-10)
  * Correct ControlRoomLayout typing

# 0.2.3 (2016-12-10)
  * Fix `prettyName` type signature

# 0.2.2 (2016-12-10)
  * Add LICENSE file (MIT)
  * Fix missing types after publish

# 0.2.1 (2016-12-10)
  * Add `getClientById` to clients API

# 0.2.0 (2016-12-10)
  * Integrate <ControlRoomLayout> (devteamreims/4ME#148 / !2)
  * Update flow-typed (external) definitions
  * Move flowtype definitions in a separate folder
  * Refactor codebase to make interactions between parts of the lib easier

# 0.1.0 (2016-12-09)
  * Add a CONTRIBUTING.md file
  * Ship flowtypes with the library : https://medium.com/@ryyppy/shipping-flowtype-definitions-in-npm-packages-c987917efb65#.u9gnn5xxf
  * Implement suggestion engine (devteamreims/4ME#153)
  * Refactor API internals (use partial application of `data`)

# 0.0.6 (2016-12-03)
  * Make `prettyName` recursive to factor as much as possible

# 0.0.5 (2016-12-03)
  * `prettyName` will try to factor our the biggest matching sector group in supplied elementary sectors

# 0.0.4 (2016-12-03)
  * `prettyName` won't throw in case of undefined argument

# 0.0.3 (2016-12-03)
  * Add preversion hook to include changelog changes

# 0.0.2 (2016-12-03)
  * Add `prettyName` function to `sectors` API

# 0.0.1 (2016-11-20)
  * Initial import

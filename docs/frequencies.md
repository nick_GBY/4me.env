# 4me.env `clients` API

## Datatypes
See [types/index.js](types/index.js).

### Frequency
Represents an ATC frequency.

## API
### getFrequencies
> :: _ -> [Frequency]

Retrieve the full list of 4ME frequencies defined in the environment.
